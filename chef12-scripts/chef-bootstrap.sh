#!/bin/bash

names=`geni-get manifest| xmlstarlet fo | xmlstarlet sel -B -t -c "//_:node" | sed -r 's/<node/\n<node/g' | sed -r "s/.*host\ name=\"([^\"]*)\".*/\1/" | sed -s "s/\..*//"`
echo "$names" | while read line ; do
  echo "Bootstrapping $line"

   SITE="unknown"
   if [[ $HOST == *"clemson"* ]]; then SITE="clemson" ; fi
   if [[ $HOST == *"utah"* ]];    then SITE="utah" ; fi
   if [[ $HOST == *"wisc"* ]];    then SITE="wisconsin" ; fi
   if [[ $HOST == *"apt"* ]];     then SITE="apt" ; fi

   if [[ $SITE == "unknown" ]]; then
     knife bootstrap "$line" -N "$line"
   else
     knife bootstrap "$line" -N "$line" -E $SITE
   fi
done

# Tests
knife status -r
knife ssh "name:*" uptime
