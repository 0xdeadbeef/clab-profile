#!/bin/bash

# This script installs Chef server and workstation locally

# Install dependencies
apt-get update
apt-get install -y wget expect curl git xmlstarlet

set +x

# Set necessary variables and paths
HOST=`hostname -f`
CHEF_ADMIN=ryanhuang
CHEF_ADMIN_PASS=`openssl rand -base64 10`
ADMIN_EMAIL=huang@cs.jhu.edu
USER_KEY=ryanhuang.pem
ORG=orderlab
ORG_KEY=orderlab.pem
REPO_DIR=/chef-repo
CREDS=/root/.chefauth
DOTCHEF=/root/.chef

# Save admin credentials
echo $CHEF_ADMIN > $CREDS
echo $CHEF_ADMIN_PASS >> $CREDS

printf "\n\n\n"
echo `date` "--- Starting to install Chef "
set -x

# Make sure only root can run the script
if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 
   exit 1
fi

# Install Chef server
cd /tmp

# Updated to the newest version on 09/09/17
wget https://packages.chef.io/files/stable/chef-server/12.17.33/ubuntu/16.04/chef-server-core_12.17.33-1_amd64.deb --directory-prefix=/tmp
dpkg -i /tmp/chef-server-core*

chef-server-ctl reconfigure
chef-server-ctl install chef-manage
chef-server-ctl reconfigure
chef-manage-ctl reconfigure --accept-license

# Setting credentials
mkdir $DOTCHEF
set +x
chef-server-ctl user-create $CHEF_ADMIN Owner Owner $ADMIN_EMAIL $CHEF_ADMIN_PASS --filename "$DOTCHEF/$USER_KEY"
chef-server-ctl org-create $ORG "Owner Group" --association_user $CHEF_ADMIN --filename "$DOTCHEF/$ORG_KEY"
set -x

# Install Chef workstation
cd /tmp

# Updated to the newest version on 09/09/17
wget https://packages.chef.io/files/stable/chefdk/2.5.3/ubuntu/16.04/chefdk_2.5.3-1_amd64.deb --directory-prefix=/tmp
dpkg -i /tmp/chefdk*
# chef verify

cat > $DOTCHEF/config.rb << END
current_dir = File.dirname(__FILE__)
log_level                :info
log_location             STDOUT
node_name                "$CHEF_ADMIN"
client_key               "#{current_dir}/$USER_KEY"
validation_client_name   "$ORG-validator"
validation_key           "#{current_dir}/$ORG_KEY"
chef_server_url          "https://`hostname -f`/organizations/$ORG"
syntax_check_cache_path  "$DOTCHEF/syntaxcache"
cookbook_path            ["$REPO_DIR/cookbooks"]
END

# Make knife trust the server's certificate
knife ssl fetch
